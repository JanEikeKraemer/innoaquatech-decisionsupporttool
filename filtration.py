"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import container as con


class BioFilter(con.Container):
    """Class Biofiltration."""

    def __init__(self, name, time, width, depth, height, target=None):
        """Initialize Biological Filter."""
        self.width = width
        self.depth = depth
        super().__init__(name, time, height)
        self.connectOverflow(target)

    def getSurface(self):
        """
        Get the BioFilter's surface area.

        :return: the surface of the BioFilter
        :rtype: int
        """
        return self.width * self.depth


class MechanicalFilter(con.Container):
    """
    Class MechanicalFilter.

    :param name: the name of the filtration
    :type name: string
    :param time: the current time
    :type time: dt.datetime
    :param width: the width of the filtration
    :type width: number
    :param depth: the depth of the filtration
    :type depth: number
    :param height: the height of the filtration
    :type height: number
    :param target: the overflow target of filtration. default = None
    :type target: container.Container
    """

    def __init__(self, name, time, width, depth, height,  target=None):
        """Initialize Mechanical Filter."""
        self.width = width
        self.depth = depth
        super().__init__(name, time, height)
        self.connectOverflow(target)
        self.dailyDiscardRate = 0.01
        self.meter = 0

    def setDailyDiscardRate(self, discardRate=0.01):
        """
        Set the percentage of water to be removed per day.
        :param discardRate: the percentage to be discarded
        :type discardRate: float
        :return: logstring
        :rtype: string
        """
        self.dailyDiscardRate = (
            self.getRoot().getTotalWaterVolume() * discardRate)
        return ("DailyDiscardRate was set to " + str(self.dailyDiscardRate))

    def discardWater(self):
        """
        Removes water according to the dailyDiscardRate
        """
        w = self.getWater()
        w.setAmount(w.getAmount() - self.dailyDiscardRate)
        self.meter += self.dailyDiscardRate
        return ("Discarded " + str(self.dailyDiscardRate * 1000) +
                " Liters of wastewater.")

    def getSurface(self):
        """
        Get the Mechanical Filter's surface area.

        :return: the surface of the Mechanical Filter
        :rtype: int
        """
        return self.width * self.depth


class Skimmer(con.Container):
    """
    Class Skimmer. This device removes organic compounds and other
    particles from the water. It uses fine air bubbles to create a foam, which
    is then removed from the system. Due to the large interface of air and
    water, the effluent water of the skimmer is saturated with oxygen.
    In the current version of this tool, water chemical processes like
    the dynamics of oxygen content and other substances are not
    modelled.
    """

    def __init__(self, name, time, width, depth, height=2.0, target=None):
        """Initialize Skimmer."""
        self.width = width
        self.depth = depth
        super().__init__(name, time, height)
        self.connectOverflow(target)

    def getSurface(self):
        """
        Get the skimmer's surface area.

        :return: the surface of the skimmer
        :rtype: int
        """
        return self.width * self.depth
