"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
from abc import ABC, abstractmethod
from math import pi

import numpy as np

import aquatics as aq
import container as con


class Tank(con.Container, ABC):
    """
    Class Tank.

    :param name: the name of the tank
    :type name: string
    :param time: the time the simulation starts
    :type time: dt.datetime
    :param height: the height of the tank in meters
    :type height: int
    """

    def __init__(self, name, time, height, wallet):
        """Initialize Tank instance."""
        super().__init__(name, time, height)
        self.wallet = wallet
        self.fishmeter = 0
        self.numberSoldFish = 0

    def getPopulation(self):
        """Returns the population instance inside this tank"""
        for i in self.getWater().getContent():
            if isinstance(i, aq.Population):
                return i
        return None

    def fillEmUp(self, p):
        """
        Buy more fingerlings.

        :param p: the population to refill
        :type p: aquatics.population
        """
        self.numberSoldFish = 0
        m = self.getRoot()
        p.size = m.calculateFishStock(p.species, self, p.stockingDensity, p.targetWeight)
        p.weights = np.random.normal(p.startWeight, 0.1 * p.startWeight, p.size)
        time = self.getGlobalTime()
        self.wallet.purchase(time, "Fingerling", p.size)

    def reducePopulation(self, p):
        """
        Reduce a specified population from a tank.

        :param p: the population to reduce
        :type p: aquatics.population
        """
        w = self.getWater()
        time = self.getGlobalTime()
        sold = [f for f in p.getWeights() if f > p.getTargetWeight()]

        p.size -= len(sold)
        p.weights = [w for w in p.getWeights() if not w in sold]
        self.wallet.sell(time, "Fish", sum(sold))
        self.fishmeter += sum(sold)
        self.numberSoldFish += len(sold)

        return ("population from " + self.name + " has been reduced by " +
                str(len(sold)) + " animals")

    def getPlotdata(self):
        data = self.getPopulation().getData()
        avgWeights = [[], []]
        sumWeights = [[], []]
        start = self.getRoot().start

        for t in data["avgWeights"]:
            avgWeights[0].append((t[0]-start).total_seconds() / 86400.0)
            avgWeights[1].append(t[1])

        for t in data["sumWeights"]:
            sumWeights[0].append((t[0]-start).total_seconds() / 86400.0)
            sumWeights[1].append(t[1])

        return avgWeights, sumWeights

    @abstractmethod
    def getSurface(self):
        """Calculate the surface, which depends on the containers shape."""
        pass



class CuboidTank(Tank):
    """
    Create a cuboid shaped tank.

    :param name: the name of the tank
    :type name: string
    :param time: the time the simulation starts
    :type time: dt.datetime
    :param width: the width of the tank in meters
    :type width: int
    :param depth: the depth of the tank in meters
    :type depth: int
    :param height: the height of the tank in meters
    :type height: int
    """

    def __init__(self, name, time, width, depth, height, wallet):
        """Initialize cuboid tank."""
        self.width = width
        self.depth = depth
        super().__init__(name, time, height, wallet)

    def getSurface(self):
        """
        Get the tanks surface area.

        :return: the surface of the tank
        :rtype: int
        """
        return self.width * self.depth


class CylinderTank(Tank):
    """
    Create a cylinder shaped tank.

    :param name: the name of the tank
    :type name: string
    :param time: the time the simulation starts
    :type time: dt.datetime
    :param radius: the radius of the tank in meters
    :type radius: int
    :param height: the height of the tank in meters
    :type height: int
    """

    def __init__(self, name, time, radius, height, wallet):
        """Initialize cylinder tank."""
        self.radius = radius
        super().__init__(name, time, height, wallet)

    def getSurface(self):
        """
        Get the tanks surface area.

        :return: the surface of the tank
        :rtype: int
        """
        return pi * self.radius**2


class Sumptank(con.Container):
    """
    Class Sumptank.

    :param name: the name of the sumptank
    :type name: string
    :param time: the time the simulation starts
    :type time: dt.datetime
    :param w: the width of the sumptank in meters
    :type w: float
    :param d: the depth of the sumptank in meters
    :type d: float
    :param h: the height of the sumptank in meters
    :type h: float
    """

    def __init__(self, name, time, w, d, h):
        """Initialize Sumptank."""
        self.width = w
        self.depth = d
        super().__init__(name, time, h)
        self.needFreshWater = False

    def getSurface(self):
        """
        Get the sumptanks surface area.

        :return: the surface of the sumptank
        :rtype: int
        """
        return self.width * self.depth
