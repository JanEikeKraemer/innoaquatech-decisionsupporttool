"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import random

import matplotlib.pyplot as plt
import numpy as np

import inlet
import outlet
import pump
import tank


def resourcePlots(ticks, executed, durations):
    """Create Plots from an array of executed events."""
    callbacks = list(set([e.callback.__name__ for e in executed]))

    n = len(callbacks)
    cmap = get_cmap(n)

    fig, ax = plt.subplots()
    fig.patch.set_facecolor((0.5, 0.5, 0.5))
    ax.set_facecolor((0.5, 0.5, 0.5))

    plt.subplot(121)
    index = np.arange(n)
    bar_width = 0.6
    opacity = 1
    bars = []

    for i in range(0, n):
        pos = i
        height = len([e for e in executed if e.callback.__name__ == callbacks[i]])
        bars.append(plt.bar(pos,
                            height,
                            bar_width,
                            alpha=opacity,
                            color=tuple(c * 0.7 for c in cmap(i)),
                            label=callbacks[i]))

    plt.title('Callback distribution')
    plt.xlabel('Callback Functions')
    plt.ylabel('Times used')
    plt.xticks(index, tuple(callbacks))
    plt.yscale('log', nonposy='clip')
    plt.legend()
    plt.tight_layout()
    plt.grid(True)

    plt.subplot(122)
    plt.title("update duration (" + str(len(ticks)) + " updates)")
    plt.ylabel("Duration in seconds")
    plt.xlabel("Timestamps")
    plt.bar(ticks, durations, width=0.02)
    plt.grid(True)


def plotElectricals(model):
    """Plot electricMeter"""
    fig, ax = plt.subplots()
    random.seed(42)

    plt.title("Energy consumption of electrical devices")
    plt.ylabel("Energy consumption in kWh")
    plt.xlabel("Time in days")

    electricals = model.getElectricals()
    ticks = electricals[0].getTicks()
    start = ticks[0]
    ticks = [(i - start).total_seconds() / 86400.0 for i in ticks]
    plotdata = [(x.name, x.getPlotdata()) for x in electricals]
    sum = [0.0 for i in ticks]

    for name, data in plotdata:
        color = "#" + ''.join([random.choice('3456789abc') for j in range(6)])
        newSum = [sum[k] + data[k] for k in range(len(sum))]
        ax.plot(ticks, newSum, color=color, label=name)
        ax.fill_between(ticks, sum, newSum, facecolor=color + "99", interpolate=True)
        sum = newSum
    ax.legend()

    plt.grid(True)
    plt.savefig("./SVG/" + str(model.id) + "/EnergyConsumption" + ".svg")
    plt.close()


def plotWallet(model):
    """Plot Wallet"""
    random.seed(42)

    wallet = model.getWallet()
    purchases, sales, balance = wallet.getPlotdata()
    start = model.start
    currency = str(model.configuration.currency)
    labels = [
        'Fingerling',
        'Freshwater',
        'Wastewater',
        'Electricity',
        'Fish feed',
        'Salt'
    ]

    colors = {}

################################################################################

    # fig, ax = plt.subplots(figsize=(16, 12))
    # plt.subplot(221)
    plt.figure()
    plt.title("Accumulated running costs by resource")
    plt.xlabel("Time in days")
    plt.ylabel("Money in " + currency)
    # plt.yscale('log', nonposy='clip')

    oldSum = []
    for p in labels:
        colors[p] = "#" + ''.join([random.choice('6789abc') for j in range(6)])
        try:
            data = purchases[p].copy()
        except:
            continue
        for e in range(1, len(data)):
            data[e] = data[e] + data[e - 1]
        if oldSum == []:  # dirty workaround
            oldSum = [0.0 for i in data]
        ticks = [i for i in range(len(data))]
        newSum = [oldSum[k] + data[k] for k in range(len(oldSum))]
        plt.plot(ticks, newSum, color=colors[p], label=p)
        plt.fill_between(ticks, oldSum, newSum, facecolor=colors[p] + "99", interpolate=True)
        oldSum = newSum
    plt.legend()
    plt.grid(True)
    plt.savefig("./SVG/" + str(model.id) + "/wallet/AccumulatedRunningCosts" + ".svg")

################################################################################

    # plt.subplot(222)
    plt.figure()
    plt.title("Total cost distribution after simulation duration")
    plt.ylabel("Money in " + currency)
    # plt.xlabel("Time in days")

    i = 1
    for k in labels:
        color = "#" + ''.join([random.choice('3456789abc') for j in range(6)])
        try:
            h = sum(purchases[k])
        except:
            continue
        bar = plt.bar(i, h, color=colors[k], label=k)
        plt.text(bar[0].get_x() + bar[0].get_width() / 2.0, h, "{:.2f}".format(h),
                 ha='center', va='bottom', color=colors[k], fontweight='bold')
        i += 1

    plt.xticks([])
    plt.legend()
    plt.grid()
    plt.savefig("./SVG/" + str(model.id) + "/wallet/TotalCostDistribution" + ".svg")

################################################################################

    # plt.subplot(223)
    plt.figure()
    plt.title("Total balance of costs and revenues")
    plt.ylabel("Money in " + currency)
    plt.xlabel("Time in days")
    # plt.plot(balance[0], balance[1], color='red', label="balance")
    pos = np.array(balance[1].copy())
    neg = np.array(balance[1].copy())
    pos[pos < 0] = np.nan
    neg[neg > 0] = np.nan

    plt.plot(balance[0], pos, color='g', linewidth=2)
    plt.plot(balance[0], neg, color='r', linewidth=2)
    plt.grid(True)
    plt.savefig("./SVG/" + str(model.id) + "/wallet/Balance" + ".svg")

################################################################################

    # plt.subplot(224)
    plt.figure()
    plt.title("Daily costs by resource")
    plt.ylabel("Money in " + currency)
    plt.xlabel("Time in days")
    plt.yscale('log', nonposy='clip')

    for k in labels:
        try:
            ticks = [i for i in range(len(purchases[k]))]
        except:
            continue
        data = purchases[k]
        plt.plot(ticks, data, color=colors[k], linewidth=2, label=k)

    plt.legend()
    plt.grid(True)
    plt.savefig("./SVG/" + str(model.id) + "/wallet/dailyCostsByResource" + ".svg")


def plotTanks(model):
    random.seed(43)

    tanks = [t for t in model.getContent() if isinstance(t, tank.Tank)]
    colors = ["#" + ''.join([random.choice('3456789abc') for j in range(6)]) for t in tanks]

################################################################################
    fig, ax = plt.subplots()
    plt.title("Total biomass by tank")
    plt.ylabel("Weight in kilogramm")
    plt.xlabel("Time in days")

    for e, t in enumerate(tanks):
        _, sumWeights = t.getPlotdata()
        ax.plot(sumWeights[0], sumWeights[1], color=colors[e], label=t.name)

    ax.legend()

    plt.grid(True)
    plt.savefig("./SVG/" + str(model.id) + "/TotalBiomassByTank" + ".svg")
    plt.close()
################################################################################
    fig, ax = plt.subplots()
    plt.title("Average Fishweight by tank")
    plt.ylabel("Weight in kg")
    plt.xlabel("Time in days")

    for e, t in enumerate(tanks):
        avgWeights, _ = t.getPlotdata()
        ax.plot(avgWeights[0], avgWeights[1], color=colors[e], label=t.name)

    ax.legend()

    plt.grid(True)
    plt.savefig("./SVG/" + str(model.id) + "/AvgWeightByTank.svg")
    plt.close()


def get_cmap(n, name='hsv'):
    return plt.cm.get_cmap(name, n)
