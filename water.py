"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import aquatics
import simObject as so


class Water(so.SimObject):
    """
    Class Water.

    :param amount: the amount of the water instance in m^3
    :type amount: int
    :param temperature: the temperature of the water instance in °C
    :type amount: float
    :param salinity: salinity of the water in PSU
    :type salinity: float
    """

    def __init__(self, amount, temperature, time, wallet, salinity):
        """Initialize water instance."""
        super().__init__(time)
        self.amount = float(amount)
        self.temperature = temperature
        self.salinity = salinity
        wallet.purchase(time, "Freshwater", amount)
        if self.salinity > 0:
            """Calculate the correct amount of salt to reach salinity and
            purchase it. A density of 996.37kg/m^3 is assumed for water
            with a temperature of 27.5 degrees Celsius and a salinity of 0"""
            wallet.purchase(time, "Salt", ((self.amount * 996.37) / 1000)
                            * self.salinity)

    def __str__(self):
        """Overwrite string func."""
        return str(self.amount * 1000) + " litres of water"

    def merge(self, water):
        """
        Merge water instance with another.

        :param water: the water to be merged with this instance
        :type water: water.Water
        """
        self.temperature = ((self.amount * self.temperature +
                             water.amount * water.temperature) /
                            (self.amount + water.amount))
        self.amount += water.amount
        self.content.extend(water.content)

    def setAmount(self, amount):
        """
        Set amount for water instance.

        :param amount: the new amount of the water instance
        :type amount: int
        """
        self.amount = amount

    def getAmount(self):
        """
        Get the amount of the water only.

        :return: the amount of the water instance without fish etc.
        :rtype: int
        """
        return self.amount

    def getVolume(self):
        """
        Get total volume of water instance.

        :return: the volume of the water instance
        :rtype: float
        """
        volume = self.amount
        fish = [f for f in self.content if isinstance(f, aquatics.Aquatic)]
        for f in fish:
            volume += f.getVolume()
        return volume

    def setTemperature(self, temperature):
        """
        Get temperature of water instance.

        :param temperature: temperature of the water instance
        :type temperature: float
        """
        self.temperature = temperature

    def getTemperature(self):
        """
        Get temperature of water instance.

        :return: temperature of the water instance
        :rytpe: float
        """
        return self.temperature
