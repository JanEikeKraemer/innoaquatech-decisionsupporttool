"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import datetime as dt

import container as con
import electrical as ec
import water

import pickle


class Pump(ec.Electrical):
    """
    Class Pump.

    :param name: the name of the pump, e.g. pump1
    :type name: string
    :param time: the time the simulation starts
    :type time: dt.datetime
    :param wallet: the wallet that pays for the electricity of the pump
    :type wallet: wallet.Wallet
    :param head: the height to which the water is pumped (m)
    :type head: float
    :param flowRate: the amount of water that the pump moves (m^3/h)
    :type flowRate: float
    :param source: the water source
    :type source: con.Container
    :param target: the target(s) for the pumped water
    :type target: con.Container
    """

    def __init__(self, name, time, wallet, head, flowRate, source, *target):
        """Initialize Pump instance."""
        super().__init__(name, time, wallet)
        self.target = []
        for i in target:
            assert(isinstance(i, con.Container)), (
                "Expected target to be a container, got" +
                str(type(target)) + "instead"
            )
            self.target.append(i)

        self.source = source
        assert(isinstance(source, con.Container)), (
            "Expected source to be a container, got" +
            str(type(source)) + "instead")

        self.flowRate = flowRate

        self.pwrConsumption = 0
        self.calculatePwrConsumption(flowRate, head)

        #self.pumpingThreshold = self.flowRate / 60
        self.newPeriodicEvent(self.pseudoPump, [12], period=dt.timedelta(hours=12),
                              silent=True)

    def calculatePwrConsumption(self, flowRate, head, efficiency=0.7, density=1000):
        """Returns the motor power consumption of a pump (kW).
        The motor power is calculated as follows:
        Pm = Ph / u
        with
        Pm = Motor power (kW)
        Ph = Hydraulic pump power (kW)
        u = pump efficiency (70%)
        and
        Ph = Q * rho * g * h / 3.6*10^6
        with
        Q = flowRate (m^3/h)
        rho = density of water (1000 kg/m^3)
        g = gravitational acceleration (9.81 m/s^2)
        h = differential head (m)
        For more information on the used calculations please visit
        https://neutrium.net/equipment/pump-power-calculation/
        """
        pwrConsumption = (((flowRate * density * 9.81 * head) / (3.6 * 10**6)) / efficiency)
        self.pwrConsumption = pwrConsumption

    def pseudoPump(self, hours):
        """
        Dummy function that lets the pump consume energy correctly, but does
        not split and merge water objects. This function is only sufficient as
        long as water chemistry is not considered in the model.
        It calculates the power consumption based on the hourly power consumption
        multiplied with the runtime in hours.
        """
        self.consume(self.pwrConsumption * hours)

    def pump(self):
        """
        Pump dummy function.

        Splits water from source and merges it into target(s) water(s).
        Here again litres could be changed to m^3
        Currently it assumes that 1 tank has 1 sumptank and 1 pump

        :return: power consumption
        :rtype: int
        """
        c = 0
        sourceWater = self.source.getWater()
        targetWater = [i.getWater() for i in self.getTarget()]
        if sourceWater.getAmount() >= self.getPumpingThreshold():

            for j in targetWater:
                j.merge(self.splitWater((self.getFlowRate() / 60)
                                        / len(targetWater)))
            c += self.pwrConsumption / 60
        self.consume(self.pwrConsumption)
        return ("pumping " + str(int((self.getFlowRate() / 60) * 1000)) +
                " liters of water from " + self.source.name +
                " into targets. Consumed a total of " +
                str(self.getTotalConsumption()) + " kWh Energy so far.")

    def splitWater(self, amount):
        """
        Split water from source.

        :param amount: the amount of water to split off
        :type amount: int
        :return: the split water
        :rtype: water.Water
        """
        sourceWater = self.getSource().getWater()
        splitWater = water.Water(amount,
                                 sourceWater.getTemperature(),
                                 self.getGlobalTime())
        if sourceWater.getAmount() <= amount:
            splitWater.setAmount(sourceWater.getAmount())
            splitWater.setContent(sourceWater.getContent())
            sourceWater.setContent([])
            sourceWater.setAmount(0)
        else:
            sourceWater.setAmount(sourceWater.getAmount() - amount)
        return splitWater

    def getFlowRate(self):
        """
        Return the flowrate of the pump

        :return: the flowrate
        :rtype: float
        """
        return self.flowRate

    def getPumpingThreshold(self):
        """
        Return the pumping threshold of the pump

        :return: the pumping threshold
        :rtype: float
        """
        return self.pumpingThreshold

    def getSource(self):
        """
        Return the attached source.

        :return: the attached source
        :rtype: object
        """
        return self.source

    def getTarget(self):
        """
        Return the attached target.

        :return: the attached target
        :rtype: object
        """
        return self.target


class AirPump(ec.Electrical):
    """
    Class AirPump.
    This device is used in a protein skimmer and works both as a pump and
    as a chemical filtration unit. It produces ozone and uses an air/ozone
    mix to produce a protein foam, which can easily be removed from the
    system. The ozone production requires a high energy input, which is the
    reason for the high power consumption of this pump in comparison with
    conventional ones.

    :param name: the name of the pump, e.g. pump1
    :type name: string
    :param time: the time the simulation starts
    :type time: dt.datetime
    :param wallet: the wallet that pays for the electricity of the pump
    :type wallet: wallet.Wallet
    :param flowRate: the amount of water that the pump moves (m^3/h)
    :type flowRate: float
    :param source: the water source
    :type source: con.Container
    :param target: the target(s) for the pumped water
    :type target: con.Container
    """

    def __init__(self, name, time, wallet, flowRate, source, *target):
        """Initialize AirPump instance."""
        super().__init__(name, time, wallet)
        self.target = []
        for i in target:
            assert(isinstance(i, con.Container)), (
                "Expected target to be a container, got" +
                str(type(target)) + "instead"
            )
            self.target.append(i)

        self.source = source
        assert(isinstance(source, con.Container)), (
            "Expected source to be a container, got" +
            str(type(source)) + "instead")

        self.flowRate = flowRate

        self.pwrConsumption = 0
        self.calculatePwrConsumption(flowRate)

        self.newPeriodicEvent(self.pseudoPump, [12], period=dt.timedelta(hours=12),
                              silent=True)

    def calculatePwrConsumption(self, flowRate):
        """Returns the power consumption of the air pump / ozone generator (kW).
        For more information see /.Pickles/pwrConsumptionAirPump.txt and
        /.Pickles/pwrConsumptionAirPump.py
        """
        pwr = 0.0174289 * flowRate + 0.47472676
        self.pwrConsumption = pwr

    def pseudoPump(self, hours):
        """
        Dummy function that lets the pump consume energy correctly, but does
        not split and merge water objects. This function is only sufficient as
        long as water chemistry is not considered in the model.
        It calculates the power consumption based on the hourly power consumption
        multiplied with the runtime in hours.
        """
        self.consume(self.pwrConsumption * hours)

    def getFlowRate(self):
        """
        Return the flowrate of the pump

        :return: the flowrate
        :rtype: float
        """
        return self.flowRate

    def getSource(self):
        """
        Return the attached source.

        :return: the attached source
        :rtype: object
        """
        return self.source

    def getTarget(self):
        """
        Return the attached target.

        :return: the attached target
        :rtype: object
        """
        return self.target
