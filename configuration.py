"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import json
import sys
from math import pi

import aquatics as aq


class Configuration():
    """
    Initialize Configuration-Instance.

    :param simDuration: the duration of the simulation in days
    :type simDuration: int
    :param tapWaterTemp: the water temperature before heating in Celsius
    :type tapWaterTemp: float
    :param targetWaterTemp: the water temperature after heating in Celsius
    :type targetWaterTemp: float
    :param tanks: the tanks of the simulation
    :type tanks: list of tanks # TODO: or list of what really?
    """

    def __init__(self,
                 currency='€',
                 dailyDiscardRate=0.01,
                 id=0,
                 salinity=0,
                 simDuration=1000,
                 tapWaterTemp=11.0,
                 targetWaterTemp=25.0,
                 tanks=[
                     #(name, (radius, heigth), species, stockingDensity, startWeight, targetWeight)
                     #(name, (width, depth, heigth), species, stockingDensity, startWeight, targetWeight)
                     #  ("Tank1", (1.5, 1.0), aq.AfricanCatfish, 400, 0.04, 1.5),
                     {
                         "Reference": "Tank_0",
                         "Species": "African Catfish",
                         "TankHeight": 1.0,
                         "TankWidth": 1.0,
                         "TankDepth": 1.0,
                         "TankDiameter": None,
                         "StockingDensity": 100,
                         "StartSpecieWeight": 0.019,
                         "TargetHarvestingWeight": 1.5
                     },
                     {
                         "Reference": "Tank_1",
                         "Species": "African Catfish",
                         "TankHeight": 1.0,
                         "TankWidth": 1.0,
                         "TankDepth": 1.0,
                         "TankDiameter": None,
                         "StockingDensity": 100,
                         "StartSpecieWeight": 0.0012,
                         "TargetHarvestingWeight": 1.5
                     }
                 ],
                 prices={
                     # pay money for:
                     'Freshwater': 1.9,
                     'Wastewater': 2.6,
                     'Electricity': 0.27,
                     'Fish feed': 1.3,
                     'Fingerling': 0.65,
                     'Salt': 2.0,
                     # get money for:
                     'Fish': 3.0
                 }
                 ):
        self.currency = currency
        self.dailyDiscardRate = dailyDiscardRate
        self.id = id
        self.prices = prices
        self.salinity = salinity
        self.simDuration = simDuration
        self.tapWaterTemp = tapWaterTemp
        self.targetWaterTemp = targetWaterTemp
        self.tanks = tanks
        try:
            self.validate()
        except AssertionError as e:
            sys.exit("Invalid configurationuration encountered")

    def validate(self):
        """Validate the configurationuration."""
        assert type(self.simDuration) is int
        assert 0 < self.simDuration <= 1200
        assert type(self.targetWaterTemp) is float
        assert type(self.tapWaterTemp) is float
        assert type(self.tanks) is list
        assert 0 < len(self.tanks) < 10, "Number of tanks out of bound"

        for i in self.tanks:
            assert type(i) is dict
            assert len(i) == 9

    def parseJson(self, jsonString, filePath=None):
        """Apply json string to configuration."""
        if filePath is not None:
            with open(filePath) as json_file:
                conf = json.load(json_file)
        else:
            conf = json.loads(jsonString)
        self.__init__(currency=conf['Currency'],
                      dailyDiscardRate=conf['DailyDiscardRate'],
                      id=conf['Id'],
                      simDuration=conf['SimDuration'],
                      salinity=conf['Salinity'],
                      tapWaterTemp=conf['FreshWaterTemperature'],
                      targetWaterTemp=conf['TargetWaterTemperature'],
                      tanks=conf['Tanks'],
                      prices=conf['Prices']
                      )
