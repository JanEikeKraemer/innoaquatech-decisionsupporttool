"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

import aquatics
import simObject as so


class Feed(so.SimObject):
    """
    Class Feed.

    :param weight: the weight of the feed instance in Kg
    :type weight: int
    :param time: the time the simulation starts
    :type time: dt.datetime
    :param density: the density of the instance in Kg/m^3
    :type density: int
    """

    def __init__(self, weight, time, density=950):
        """Initialize Feed instance."""
        super().__init__(time)
        self.weight = weight
        self.density = density  # kg/m^3
        self.volume = self.weight / self.density
        self.events = []

    def __str__(self):
        """Print feed."""
        return "This is" + str(self.getWeight()) + " Kg of feed"

    def updateEvents(self):
        """Generate new events."""
        if isinstance(self.getParent(), aquatics.Aquatic):
            if self.eventExists(self.rot):
                self.setEvents([e for e in self.getEvents()
                                if not e.callback == self.rot])
        super().updateEvents()

    def setWeight(self, weight):
        """
        Set the weight of feed instance.

        :param weight: the new weight
        :type weight: float
        """
        self.weight = weight
        self.volume = self.weight / self.density

    def getWeight(self):
        """
        Return the current weight.

        :return: the weight of the food instance
        :rtype: int
        """
        return self.weight

    def setVolume(self, volume):
        """
        Set the volume of feed instance.

        :param volume: the new volume
        :type volume: float
        """
        self.volume = volume
        self.weight = self.volume * self.density

    def getVolume(self):
        """
        Return the volume (m^3) of the object.

        :return: the volume of the food instance
        :rtype: int
        """
        return self.volume
