"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import datetime as dt

import container as con
import simObject as so
import water


def setTemperature(t):
    """
    Set the temperature for all Taps.

    :param t: the new temperature
    :type t: number
    """
    Tap.waterTemperature = t


class Tap(so.SimObject):
    """
    Class Tap.

    :param target: the target for the tap
    :type target: con.container
    :param heater: the attached heater
    :type heater: heater.heater
    :param waterTemperature: the temperature that tapwater has (before heating)
    :type waterTemperature: float
    """

    waterTemperature = 12.0

    def __init__(self, target, heater, time):
        """Initialize Tap."""
        super().__init__(time)
        assert(isinstance(target, con.Container)), (
            "Expected target to be a container, got" +
            str(type(target)) + "instead"
        )
        self.target = target
        self.heater = heater

    def updateEvents(self):
        """
        Pours water into the target if the water level drops below 60 percent of the
        target's capacity. Fills up the target to 95 percent of it's capacity.
        """
        if not self.eventExists(self.pour):

            if (self.getTarget().getWater().getVolume() <
                    (self.getTarget().getCapacity() * 0.6)):
                self.newEvent(
                    self.pour, [], dt.timedelta(seconds=42), silent=False)
        super().updateEvents()

    def pour(self):
        """
        Fill the target container to threshold (percentage of container's
        capacity) with water.

        :param amount: the amount of water to emit
        :type amount: water.Water
        :return: a string for logging
        :rtype: string
        """
        t = self.getTarget()
        w = t.getWater()
        previousWaterVolume = w.getVolume()
        waterDifference = float(t.getCapacity() - previousWaterVolume)
        newWater = water.Water(waterDifference,
                               self.waterTemperature,
                               self.getGlobalTime())
        self.heater.heat(newWater)
        w.merge(newWater)
        return ("poured " + str(waterDifference * 1000) +
                " litres of water into " + self.target.name + ". Previous "
                + " water volume was " + str(previousWaterVolume * 1000)
                + " liters.")

    def setTarget(self, target):
        """
        Set a new target for the faucet.

        :param target: the new target
        :type target: con.container
        """
        assert(isinstance(target, con.Container)), (
            "Expected target to be a container, got" +
            str(type(target)) + "instead"
        )
        self.target = target

    def getTarget(self):
        """
        Return the target instance of the faucet.

        :return: the target of the faucet
        :rtype: con.container
        """
        return self.target
