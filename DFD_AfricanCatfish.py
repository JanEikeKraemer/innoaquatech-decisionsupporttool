"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import pickle

import numpy as np
from scipy.optimize import curve_fit

# Load Dataset from raw data text file
x = np.loadtxt('DFD_AfricanCatfish.txt', usecols=(0,), skiprows=(26))
y = np.loadtxt('DFD_AfricanCatfish.txt', usecols=(1,), skiprows=(26))


def logReg(x, a=-0.67254091, b=1.09924419):
    return a * np.log(x) + b


# Perform curve_fit and save values for best fit
popt, pcov = curve_fit(logReg, x, y)

# Save fit function with the best fit values as default into variable
DailyFeedDemand = logReg
# Create and open file to store fit function
a = open("pickle_DFD_AfricanCatfish", 'wb')
# Store fit function as pickle in file
pickle.dump(DailyFeedDemand, a)
# Close created file
a.close()
# In order to load the fit function in any other project, copy the file
# "pickle_DFD_AfricanCatfish" into the project folder and open it by
# using the command
# pickle.load(open("pickle_DFD_AfricanCatfish", 'rb'))

"""
The following code is for the purpose of debugging,
evaluation and reproducibility:
"""


def getRsquared(f, ydata, xdata, popt):
    """
    Get information about the quality of the fit function.
    returns R²-value
    """
    residuals = ydata - f(xdata, popt[0], popt[1])
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((ydata - np.mean(ydata))**2)
    r_squared = 1 - (ss_res / ss_tot)
    return r_squared

# Uncomment next line to show Evaluation result
#print("Quality of the curve_fit: R² = ", getRsquared(logReg, y, x, popt))


# Uncomment next four lines to print example results for the fit function
# DFD = pickle.load(open(
#     "pickle_DFD_AfricanCatfish",
#     'rb'))
# print(DFD(0.01), DFD(0.02), DFD(0.03), DFD(0.3), DFD(0.5), DFD(1.0), DFD(1.75), DFD(1.8))
