"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
from abc import ABC, abstractmethod

import simObject as so
import water


class Container(so.SimObject, ABC):
    """
    Class Container.

    :param name: the containers name
    :type name: string
    :param time: the simulation time
    :type time: dt.datetime
    :param height: the containers height
    :type height: int
    """

    def __init__(self, name, time, height):
        """Initialize Container instance."""
        super().__init__(time)
        self.name = name
        self.height = height
        self.capacity = self.getSurface() * self.height
        self.events = []
        self.overflowTarget = []

    def updateEvents(self):
        """Create new events."""
        if self.isOverfilled():
            if not self.eventExists(self.overflow):
                self.newEvent(self.overflow, silent=True)

        super().updateEvents()

    def connectOverflow(self, *target):
        """
        :param target: object where overflowing water flows to.
        If multiple targets are chosen
        the water will flow into each target with the same flow rate
        (overflow devided by the amount of targets).
        :type target: list of Container
        """
        for i in target:
            self.overflowTarget.append(i)

    def overflow(self):
        """
        Reduce water volume to maximum capacity of the container.

        :return: Information of how much water overflowed (for logging)
        :rtype: string
        """
        w = self.getWater()
        if self.isOverfilled():
            overflowVolume = w.getVolume() - self.getCapacity()

            numberTargets = len(self.overflowTarget)
            if numberTargets >= 1:
                for i in self.overflowTarget:
                    overflowWater = water.Water(
                        (overflowVolume / numberTargets),
                        w.getTemperature(),
                        w.getGlobalTime())
                    i.getWater().merge(overflowWater)
            w.setAmount(w.getAmount() - overflowVolume)
        return (str(overflowVolume * 1000) + " Liters of water schwapping from "
                + str(self) + " into" + str(self.overflowTarget))

    def isOverfilled(self):
        """
        Check if Capacity is exceeded.

        :return: True if the container is overfilled, False otherwise
        :rtype: bool
        """
        return self.getWater().getVolume() - self.getCapacity() > 0.001

    def getName(self):
        """
        Get the name of the container.

        :return: the name of the container
        :rtype: str
        """
        return self.name

    def getWater(self):
        """
        Return water instance of container.

        :return: the water instance inside the container
        :rtype: water.Water
        """
        w = [x for x in self.content if isinstance(x, water.Water)]
        assert len(w) == 1, "expected 1 water, got " + str(len(w)) + " instead"
        return w[0]

    def getCapacity(self):
        """
        Get the tanks capacity.

        :return: capacity of the tank
        :rtype: int
        """
        return self.capacity

    def getOverflowTargets(self):
        """
        Get the overflowtarget(s).

        :return: overflowtargets
        :rtype: list
        """
        return self.overflowTarget

    @abstractmethod
    def getSurface(self):
        """Calculate the surface, which depends on the containers shape."""
        pass
