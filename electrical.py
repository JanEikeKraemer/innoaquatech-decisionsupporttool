"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import datetime as dt

import simObject as so


class Electrical(so.SimObject):
    """docstring for Electrical."""

    def __init__(self, name, time, wallet):
        super().__init__(time)
        self.name = name
        self.wallet = wallet
        self.electricMeter = 0
        self.newPeriodicEvent(self.measureElectricMeter, [], dt.timedelta(days=1),
                              silent=True)
        self.data["electrical meter of " + self.name] = [(0, self.getTime())]

    def measureElectricMeter(self):
        """Read and save electricMeter."""
        time = self.getGlobalTime()
        pair = (self.electricMeter, time)
        self.data["electrical meter of " + self.name].append(pair)

    def consume(self, amount):
        self.electricMeter += amount
        time = self.getGlobalTime()
        self.wallet.purchase(time, "Electricity", amount)

    def getPlotdata(self):
        """Return data as list"""
        plotdata = [x[0] for x in self.data["electrical meter of " + self.name]]
        return plotdata

    def getTicks(self):
        """Return ticks as list"""
        ticks = [x[1] for x in self.data["electrical meter of " + self.name]]
        return ticks
