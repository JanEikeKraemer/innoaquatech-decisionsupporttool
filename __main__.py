"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import json
import sys

import configuration as cfg
import model

# this is a python dictionary. jsons string is in test.txt
# exampleJSON = {
#     "Id": 1237,
#     "Currency": "€",
#     "FreshWaterTemperature": 10.0,
#     "TargetWaterTemperature": 27.5,
#     "DailyDiscardRate": 0.01,
#     "Salinity": 15,
#     "SimDuration": 300,
#     "Tanks": [
#         {
#             "Reference": "Tank nr. 1",
#             "Species": "WhiteLegShrimp",
#             "TankHeight": 0.5,
#             "TankWidth": 5.0,
#             "TankDepth": 5.0,
#             "TankDiameter": None,
#             "StockingDensity": 200,
#             "StartSpecieWeight": 0.001,
#             "TargetHarvestingWeight": 0.03
#         },
#         {
#             "Reference": "Tank nr. 2",
#             "Species": "WhiteLegShrimp",
#             "TankHeight": 0.5,
#             "TankWidth": 5.0,
#             "TankDepth": 5.0,
#             "TankDiameter": None,
#             "StockingDensity": 200,
#             "StartSpecieWeight": 0.002,
#             "TargetHarvestingWeight": 0.03
#         }
#     ],
#     "Prices": {
#         'Freshwater': 1.9,
#         'Wastewater': 2.6,
#         'Electricity': 0.27,
#         'Fish feed': 1.3,
#         'Fingerling': 0.10,
#         'Salt': 1.0,
#         'Fish': 40.0
#     }
# }
#
# # write to file like that:
# with open('test.txt', 'w') as outfile:
#     json.dump(exampleJSON, outfile)

# read from file like this (as json string):
# with open('test.txt') as json_file:
#     jsonString = json.dumps(json.load(json_file))

try:
    pathToJsonString = sys.argv[1]
    conf = cfg.Configuration()
    conf.parseJson(None, pathToJsonString)  # parse from file
    # conf.parseJson(jsonString) # parse json string directly
except:
    sys.exit("No filepath to valid json string specified")
m = model.Model(conf)
numbers = m.simulate(True)
m.makePlots()
userID = str(conf.id)
with open(userID+'_results.txt', 'w') as outfile:
    json.dump(numbers, outfile)
