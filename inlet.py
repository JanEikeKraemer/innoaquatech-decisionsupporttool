"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import datetime as dt

import matplotlib.pyplot as plt

import simObject as so
import tank
import water


class Inlet(so.SimObject):
    """
    This class handles freshwater input into the system. If needed, it also heats up the
    water and adds salt to it.
    :param target: the target for the tap
    :type target: con.container
    :param heater: the attached heater
    :type heater: heater.heater
    :param waterTemperature: the temperature that tapwater has (before heating)
    :type waterTemperature: float
    """

    def __init__(self, target, heater, tapWaterTemp, targetWaterTemp,
                 salinity, time, wallet):
        """Initialize inlet."""
        super().__init__(time)
        assert(isinstance(target, tank.Sumptank)), (
            "Expected target to be a sumptank, got" +
            str(type(target)) + "instead")
        self.heater = heater
        self.meter = 0
        self.data['freshwaterVolume'] = [self.meter]
        self.data['freshwater_ticks'] = [0]
        self.data['freshwaterPrices'] = [0]
        self.data['freshwaterCosts'] = [0]

        self.salinity = salinity
        self.target = target
        self.targetWaterTemp = targetWaterTemp
        self.tapWaterTemp = tapWaterTemp
        self.wallet = wallet
        self.saltmeter = 0

    def updateEvents(self):
        """
        Pours water into the target if the water level drops below 60 percent of the
        target's capacity. Fills up the target to 95 percent of it's capacity.
        """
        if not self.eventExists(self.pour):
            if self.target.needFreshWater == True:
                self.newEvent(
                    self.pour, [], dt.timedelta(seconds=42), silent=False)
                self.target.needFreshWater = False
        super().updateEvents()

    def pour(self):
        """
        Fill the target container to threshold (percentage of container's
        capacity) with water.

        :param amount: the amount of water to emit
        :type amount: water.Water
        :return: a string for logging
        :rtype: string
        """
        t = self.getTarget()
        w = t.getWater()
        previousWaterVolume = w.getVolume()
        waterDifference = float(t.getCapacity() - previousWaterVolume)
        newWater = water.Water(waterDifference,
                               self.tapWaterTemp,
                               self.getGlobalTime(),
                               self.wallet, self.salinity)
        self.heater.heat(newWater)
        w.merge(newWater)
        self.meter += waterDifference
        self.data["freshwaterVolume"].append(self.meter)
        self.data["freshwater_ticks"].append(self.getRoot().currentTick)
        self.saltmeter += (((float(waterDifference) * 996.37) / 1000)
                        * self.salinity)
        return ("Poured " + str(waterDifference * 1000) +
                " litres of water into " + self.target.name + ". Previous "
                + " water volume was " + str(previousWaterVolume * 1000)
                + " liters.")

    def plot(self, ticks):
        """."""
        plt.figure()
        data = self.getData()
        freshTicks = [ticks[i] for i in data["freshwater_ticks"]]
        plt.title("Freshwater volume")
        plt.ylabel("Water volume in m^3")
        plt.xlabel("Time in days")
        plt.plot(freshTicks, data["freshwaterVolume"], "r-")
        plt.grid(True)
        plt.savefig("./SVG/freshwater.svg")

    def setTarget(self, target):
        """
        Set a new target for the faucet.

        :param target: the new target
        :type target: con.container
        """
        assert(isinstance(target, con.Container)), (
            "Expected target to be a container, got" +
            str(type(target)) + "instead"
        )
        self.target = target

    def getTarget(self):
        """
        Return the target instance of the faucet.

        :return: the target of the faucet
        :rtype: con.container
        """
        return self.target
