"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import matplotlib.pyplot as plt

import electrical as ec
import water


class Heater(ec.Electrical):
    """Class Heater."""

    def __init__(self, name, time, targetWaterTemp, wallet):
        """Initialize Heater instance."""
        super().__init__(name, time, wallet)
        self.pwrConsumption = 4200 / 3600
        self.targetWaterTemp = targetWaterTemp
        self.newEvent(self.heatSystemWater, [])

    def heat(self, water):
        """
        Heat up the water.

        :param water: the water to be heated
        :type water: water.Water
        """
        if self.targetWaterTemp > water.temperature:
            tempDif = self.targetWaterTemp - water.temperature
            water.setTemperature(self.targetWaterTemp)
            amount = water.amount * self.pwrConsumption * tempDif * 1.0526
            self.consume(amount)

    def heatSystemWater(self):
        """
        Heats the water of the whole system to the target temperature.
        """
        w = [i for i in self.getInstances() if isinstance(i, water.Water)]
        for i in w:
            self.heat(i)
        return ("Water in the system was heated to " + str(
            self.targetWaterTemp) + " °C")

    def setTargetWaterTemp(self, targetWaterTemp):
        """
        Change the target temperature.

        :param targetWaterTemp: the new target heat
        :type targetWaterTemp: float
        """
        self.targetWaterTemp = targetWaterTemp

    def plot(self, ticks):
        """."""
        data = self.getData()
        heatTicks = [ticks[i] for i in data["heater_ticks"]]
        plt.subplot(111)
        plt.title("Heater energy consumption")
        plt.ylabel("Energy consumption in kWh")
        plt.xlabel("Time in days")
        plt.plot(heatTicks, data["heater energy"], "g-")
        plt.grid(True)
