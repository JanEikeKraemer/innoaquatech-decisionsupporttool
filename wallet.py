"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""


class Wallet():
    """
    This class contains data of all purchases and sales.

    :param resources: a list of resources and their price per unit
    :type resources: list of (string, number) tuples
    """

    def __init__(self, time, resources, balance=0):
        self.resources = resources
        self.purchases = []
        self.sales = []
        self.balance = balance
        self.history = [(time, balance)]

    def purchase(self, time, resource, amount):
        t = self.makeTripel(time, resource, amount)
        self.balance -= t[2]
        if time == self.history[-1][0]:
            self.history[-1] = (time, self.balance)
        else:
            self.history.append((time, self.balance))
        self.purchases.append(t)

    def sell(self, time, resource, amount):
        t = self.makeTripel(time, resource, amount)
        self.balance += t[2]
        if time == self.history[-1][0]:
            self.history[-1] = (time, self.balance)
        else:
            self.history.append((time, self.balance))
        self.sales.append(t)

    def makeTripel(self, time, resource, amount):
        r = self.getResource(resource)
        total = r * amount
        return (time, resource, total)

    def getResource(self, name):
        return self.resources[name]

    def getPlotdata(self):

        def getCoord(a, b):
            assert a[0] != b[0] and b[1]-a[1] != 0
            m = (b[1]-a[1]) / (b[0]-a[0])
            x = (m*a[0]- a[1]) / m
            return (x, 0)

        start = self.purchases[0][0]
        for e,i in enumerate(self.purchases):
            x = (i[0] - start).total_seconds() / 86400.0
            self.purchases[e] = (x, i[1], i[2])

        days = set([int(x[0]) for x in self.purchases])
        labels = set([x[1] for x in self.purchases])

        purchases = {}
        for l in labels:
            purchases[l] = []
            for d in days:
                purchases[l].append(0)

        for p in self.purchases:
            purchases[p[1]][int(p[0])] += p[2]

        sales = {}
        for i in self.sales:
            key = i[1]
            try:
                sales[key][0].append(i[0])
                sales[key][1].append(i[2])
            except KeyError:
                sales[key] = [[i[0]], [i[2]]]

        start = self.history[0][0]
        for e,i in enumerate(self.history):
            x = (i[0] - start).total_seconds() / 86400.0
            self.history[e] = (x, i[1])

        toAdd = []
        for i in range(len(self.history)-1):
            a = self.history[i]
            b = self.history[i+1]
            if a[1]*b[1] < 0:
                toAdd.append(getCoord(a, b))

        self.history.extend(toAdd)
        self.history.sort()
        # put in plottable shape
        balance = [[], []]
        for i in self.history:
            balance[0].append(i[0])
            balance[1].append(i[1])

        return purchases, sales, balance
