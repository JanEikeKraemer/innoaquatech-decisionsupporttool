"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import datetime as dt

import matplotlib.pyplot as plt

import container as con
import simObject as so


class Outlet(so.SimObject):
    """
    This class handles water output off the system.

    :param source: the source of the outlet.
    :type source: container.Container
    :param time: the current time
    :type time: dt.datetime
    """

    def __init__(self, source, time, dailyDiscardRate, wallet):
        """Initialize Outlet"""
        super().__init__(time)
        assert(isinstance(source, con.Container)), (
            "Expected source to be a container, got" +
            str(type(source)) + "instead")
        self.dailyDiscardRate = dailyDiscardRate
        self.totalDailyDiscardRate = 0
        self.meter = 0
        self.wallet = wallet
        self.data['wastewaterVolume'] = [self.meter]
        self.data['wastewater_ticks'] = [0]

        self.newEvent(self.setDailyDiscardRate, [self.dailyDiscardRate])
        self.newPeriodicEvent(self.discardWater,
                              [],
                              dt.timedelta(hours=24),
                              dt.timedelta(minutes=3))
        self.source = source

    def setDailyDiscardRate(self, discardRate):
        """
        Set the percentage of water to be removed per day.
        :param discardRate: the percentage to be discarded
        :type discardRate: float
        :return: logstring
        :rtype: string
        """
        self.totalDailyDiscardRate = (
            self.getRoot().getTotalWaterVolume() * discardRate)
        return ("DailyDiscardRate was set to " + str(self.dailyDiscardRate))

    def discardWater(self):
        """
        Removes water according to the dailyDiscardRate
        """
        w = self.source.getWater()
        dif = self.totalDailyDiscardRate
        w.setAmount(w.getAmount() - dif)
        self.meter += dif
        self.data["wastewaterVolume"].append(self.meter)
        self.data["wastewater_ticks"].append(self.getRoot().currentTick)
        self.source.needFreshWater = True

        time = self.getGlobalTime()
        self.wallet.purchase(time, "Wastewater", dif)

        return ("Discarded " + str(dif * 1000) +
                " Liters of wastewater.")

    def plot(self, ticks):
        """."""
        plt.figure()
        data = self.getData()
        wasteTicks = [ticks[i] for i in data["wastewater_ticks"]]
        plt.title("Wastewater volume")
        plt.ylabel("Water volume in m^3")
        plt.xlabel("Time in days")
        plt.plot(wasteTicks, data["wastewaterVolume"], "r-")
        plt.grid(True)
        plt.savefig("./SVG/wastewater.svg")
