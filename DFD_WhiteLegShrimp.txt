"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
Experimental dataset from
Pilot facility 1, InnoAquaTech Project, 2019

Bodyweight(kg) DailyFeedDemand(% of Bodyweight)
0.001 14.0
0.002	8.2
0.003	6.2
0.004	5.2
0.005	4.5
0.006	3.9
0.007	3.6
0.008	3.3
0.009	3
0.01	2.8
0.011	2.6
0.012	2.5
0.013	2.3
0.014	2.2
0.015	2.1
0.016	2.0
0.017	2.0
0.018	1.9
0.019	1.8
0.020	1.8
0.021	1.8
0.022	1.8
