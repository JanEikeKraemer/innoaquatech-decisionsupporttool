"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import pickle

import numpy as np
from scipy.optimize import curve_fit


# Default values of this function are the result of the curve_fit function
def power(x, a=0.11272336, b=-0.69577089):
    return a * x ** b


def getRsquared(f, ydata, xdata, popt):
    """
    Get information about the quality of the fit function.
    returns R²-value
    """
    residuals = ydata - f(xdata, popt[0], popt[1])
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((ydata - np.mean(ydata))**2)
    r_squared = 1 - (ss_res / ss_tot)
    return r_squared


def initDFD():

    # Load Dataset from raw data text file
    x = np.loadtxt('DFD_WhiteLegShrimp.txt', usecols=(0,), skiprows=(24))
    y = np.loadtxt('DFD_WhiteLegShrimp.txt', usecols=(1,), skiprows=(24))
    # Perform curve_fit and save values for best fit
    popt, pcov = curve_fit(power, x, y)

    # Save fit function with the best fit values as default into variable
    DailyFeedDemand = power
    # Create and open file to store fit function
    a = open("pickle_DFD_WhiteLegShrimp", 'wb')
    # Store fit function as pickle in file
    pickle.dump(DailyFeedDemand, a)
    # Close created file
    a.close()
    # In order to load the fit function in any other project, copy the file
    # "pickle_DFD_WhiteLegShrimp" into the project folder and open it by
    # using the command
    # pickle.load(open("pickle_DFD_WhiteLegShrimp", 'rb'))
    # Uncomment next line to show Evaluation result
    # print("Quality of the curve_fit: R² = ", getRsquared(power, y, x, popt))

    # Uncomment next four lines to print example results for the fit function
    # DFD = pickle.load(open(
    #     "pickle_DFD_WhiteLegShrimp",
    #     'rb'))
    # print(DFD(0.001), DFD(0.002), DFD(0.005), DFD(0.01), DFD(0.015), DFD(0.02), DFD(0.025), DFD(0.03))
