"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import pickle

import numpy as np
from scipy.optimize import curve_fit


# Default values of this function are the result of the curve_fit function
def linear(x, a=0.0174289, b=0.47472676):
    return a * x + b


def getRsquared(f, ydata, xdata, popt):
    """
    Get information about the quality of the fit function.
    returns R²-value
    """
    residuals = ydata - f(xdata, popt[0], popt[1])
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((ydata - np.mean(ydata))**2)
    r_squared = 1 - (ss_res / ss_tot)
    return r_squared


def initCurveFit():

    # Load Dataset from raw data text file
    x = np.loadtxt('pwrConsumptionAirPump.txt', usecols=(0,), skiprows=(25))
    y = np.loadtxt('pwrConsumptionAirPump.txt', usecols=(1,), skiprows=(25))
    # Perform curve_fit and save values for best fit
    popt, pcov = curve_fit(linear, x, y)

    # Save fit function with the best fit values as default into variable
    pwrConsumption = linear
    # Create and open file to store fit function
    a = open("pickle_pwrConsumptionAirPump", 'wb')
    # Store fit function as pickle in file
    pickle.dump(pwrConsumption, a)
    # Close created file
    a.close()
    # In order to load the fit function in any other project, copy the file
    # "pickle_DFD_WhiteLegShrimp" into the project folder and open it by
    # using the command
    # pickle.load(open("pickle_DFD_WhiteLegShrimp", 'rb'))
    # Uncomment next line to show Evaluation result
    # print("Quality of the curve_fit: R² = ", getRsquared(linear, y, x, popt))

    # Uncomment next four lines to print example results for the fit function
    pwr = pickle.load(open(
        "pickle_pwrConsumptionAirPump",
        'rb'))
    # print(pwr(3), pwr(5), pwr(10), pwr(20), pwr(50), pwr(75))
