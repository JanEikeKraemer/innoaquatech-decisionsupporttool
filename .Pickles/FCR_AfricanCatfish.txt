"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
Dataset from
"Palm et al. 2018 - Proportional up scaling of African catfish
(Clarias gariepinus Burchell, 1822) commercial recirculating aquaculture
systems disproportionally affects nutrient dynamics"
Bodyweight(kg) FeedConversionRatio(dimensionless)
0.1	0.8
0.11	0.8
0.12	0.8
0.13	0.8
0.14	0.8
0.15	0.8
0.16	0.8
0.17	0.8
0.18	0.8
0.19	0.8
0.2	0.8
0.4	0.9
0.41	0.9
0.42	0.9
0.43	0.9
0.44	0.9
0.45	0.9
0.46	0.9
0.47	0.9
0.48	0.9
0.49	0.9
0.5	0.9
0.51	0.9
0.52	0.9
0.53	0.9
0.54	0.9
0.55	0.9
0.56	0.9
0.57	0.9
0.58	0.9
0.59	0.9
0.6	0.9
0.9	1
0.91	1
0.92	1
0.93	1
0.94	1
0.95	1
0.96	1
0.97	1
0.98	1
0.99	1
1	1
1.01	1
1.02	1
1.03	1
1.04	1
1.05	1
1.06	1
1.07	1
1.08	1
1.09	1
1.1	1
1.11	1
1.12	1
1.13	1
1.14	1
1.15	1
1.16	1
1.17	1
1.18	1
1.19	1
1.2	1
