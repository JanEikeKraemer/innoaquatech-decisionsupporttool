"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import numpy as np
import pickle

# Load Dataset from raw data text file
BodyWeight = np.loadtxt(
    'FCR_AfricanCatfish.txt', usecols=(0,), skiprows=(25))
FeedConversionRatio = np.loadtxt(
    'FCR_AfricanCatfish.txt', usecols=(1,), skiprows=(25))

# Create Polynomial fit function from Dataset
FCR = np.poly1d(np.polyfit(BodyWeight, FeedConversionRatio, 1))
# Create and open file to store PolyFit function
a = open("pickle_FCR_AfricanCatfish", 'wb')
# Store PolyFit function as pickle in file
pickle.dump(FCR, a)
# Close created file
a.close()
