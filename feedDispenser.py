"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import datetime as dt

import numpy as np

import container as con
import feed
import simObject as so
import water


class FeedDispenser(so.SimObject):
    """
    Class FeedDispenser.

    :param name: the name of the device
    :type name: string
    :param target: the target for the FeedDispenser
    :type target: con.container
    :param time: simulation time
    :type time: dt.datetime
    """

    # TODO: needs source
    def __init__(self, name, target, time, wallet):
        """Initialize FeedDispenser."""
        super().__init__(time)
        assert(isinstance(target, con.Container)), (
            "Expected target to be a container, got" +
            str(type(target)) + "instead")
        self.name = name
        self.target = target
        self.wallet = wallet
        self.newPeriodicEvent(self.insertFeed, [],
                              dt.timedelta(days=1), offset=dt.timedelta(days=1))
        self.meter = 0
        self.data["feedVolume"] = [self.meter]
        self.data["feed_ticks"] = [0]

    def insertFeed(self):
        """
        Insert feed into the target.
        :return: logstring
        :rtype: string
        """
        w = self.target.getWater()
        assert(type(w) is water.Water)
        pop = self.target.getPopulation()
        feedAmount = 0
        if pop.getSize() > 0:
            feedAmount = np.nansum(pop.getDFDs())
            w.addContent(feed.Feed(feedAmount, self.getGlobalTime()))
            self.meter += feedAmount
            self.data["feedVolume"].append(self.meter)
            self.data["feed_ticks"].append(self.getRoot().currentTick)
            time = self.getGlobalTime()
            self.wallet.purchase(time, "Fish feed", feedAmount)
            return ("Inserted " + str(feedAmount) +
                    " Kg of feed into " + self.target.name)
        return ("Inserted nothing into " + self.target.name)

    def setTarget(self, target):
        """
        Set a new target for the FeedDispenser.

        :param target: the new target
        :type target: con.container
        """
        assert(isinstance(target, con.Container)), (
            "Expected target to be a container, got" +
            str(type(target)) + "instead")
        self.target = target

    def getTarget(self):
        """
        Return the target instance of the FeedDispenser.

        :return: the target of the faucet
        :rtype: con.container
        """
        return self.target
