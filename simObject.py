"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import datetime as dt

import event


class SimObject():
    """
    Class SimObject.

    :param time: the time that the simulation begins
    :type time: dt.datetime
    """

    def __init__(self, time):
        """Initialize SimObject."""
        self.time = time
        self.events = []
        self.parent = None
        self.content = []
        self.data = {}

    def __del__(self):
        """Destructor for simObject."""
        if self.data != {}:
            m = self.getRoot()
            m.data.update(self.getData())

    def update(self):
        """
        Update an instance of an object and all its content.

        :param globalTime: all events matching this timestamp will be executed
        :type globalTime: dt.datetime
        :return: all events that were executed
        :rtype: list of event.Event
        """
        globalTime = self.getGlobalTime()
        executed = []
        for i in self.getContent():
            executed.extend(i.update())

        if self.getTime() == globalTime:
            for i in self.getEvents():
                if i.getTime() == globalTime:
                    i.execute()
                    executed.append(i)
                    if (isinstance(i, event.Periodic)
                            or isinstance(i, event.Cyclic)):
                        i.nextOccurance()
                    else:
                        self.setEvents(
                            [e for e in self.getEvents() if e is not i])
        return executed

    def updateEvents(self):
        """
        Recursively generate new events in children of this object, based on
        the class-specific local updateEvents function.
        Function must always end with
        super().updateEvents()
        """
        for i in self.getContent():
            i.updateEvents()
        self.setTime(self.getNextTimestamp())

    def eventExists(self, function):
        """
        Check if a certain function is queued already.

        :param function: the function to look for
        :type function: function
        :return: True if there there is at least one event with
            callback function, false otherwise
        :rtype: bool
        """
        return 0 < len([e for e in self.events if e.callback == function])

    def addContent(self, *new):
        """
        Append new simobject(s) to content and set parent accordingly.

        :param new: the elements to be added to an instances content
        :type new: list of objects
        """
        for i in new:
            self.content.append(i)
            i.setParent(self)

    def getData(self):
        """
        Return the data field of an object.

        :returns: the data
        :rtype: dictionary
        """
        return self.data

# # # # # # # # Event  creation # # # # # # # # # #

    def newEvent(self, callback, args=[], offset=dt.timedelta(seconds=1),
                 silent=False):
        """
        Create a new Event.

        :param callback: the function to execute
        :type callback: function
        :param args: the arguments for the callback function
        :type args: list of objects
        :param offset: The timespan between current datestamp and
            execution time
        :type offset: dt.timedelta
        :param silent: wether or not to log the event
        :type silent: bool
        """
        assert offset > dt.timedelta(0) 
        t = self.getGlobalTime() + offset
        e = event.Event(callback, args, t, silent)
        self.events.append(e)

    def newPeriodicEvent(self, callback, args, period,
                         offset=dt.timedelta(seconds=1), silent=False):
        """
        Create a new Periodic Event.

        :param callback: the function to execute
        :type callback: function
        :param args: the arguments for the callback function
        :type args: list of objects
        :param period: the period in which callback is repeated
        :type period: dt.timedelta
        :param offset: The timespan between current datestamp and
            execution time
        :type offset: dt.timedelta
        :param silent: wether or not to log the event
        :type silent: bool
        """
        assert offset > dt.timedelta(0)
        t = self.getGlobalTime() + offset
        e = event.Periodic(callback, args, t, period, silent)
        self.events.append(e)

    def newCyclicEvent(self, callback, args, cycle,
                       offset=dt.timedelta(seconds=1), silent=False):
        """
        Create a new Cyclic Event.

        :param callback: the function to execute
        :type callback: function
        :param args: the arguments for the callback function
        :type args: list of objects
        :param cycle: the cycle of times to execute callback
        :type cycle: list of dt.timedelta
        :param offset: The timespan between current datestamp and
            execution time
        :type offset: dt.timedelta
        :param silent: wether or not to log the event
        :type silent: bool
        """
        assert offset > dt.timedelta(0)
        t = self.getGlobalTime() + offset
        e = event.Cyclic(callback, args, t, cycle, silent)
        self.events.append(e)

    def getInstances(self, root=None):
        """
        Generate all Instances in hierarchy tree.

        to use global use root=self.getRoot()
        """
        instances = []
        if root is None:
            root = self.getRoot()
        else:
            assert isinstance(root, SimObject)
        frontier = [root]
        while len(frontier) > 0:
            a = frontier[0]
            frontier.extend(a.getContent())
            instances.append(a)
            frontier.remove(a)
        return instances

# # # # # # # # Setters and Getters # # # # # # # #
    def getObjectTimes(self):
        """
        Return all upcoming execution times in bottom-up order.

        :return: all timestamps of anything in content recursively
        :rtype: list of dt.datetime
        """
        times = []
        times.append(self.getTime())
        for i in self.getContent():
            times.extend(i.getObjectTimes())
        return times

    def getEventQueue(self):
        """
        Return all upcoming events in correct order.

        :return: all upcoming events
        :rtype: list of quadruples (source name, time, function name, silent)
        """
        events = []
        for e in self.getEvents():
            events.append(
                (type(self).__name__, e.getTime(),
                 e.callback.__name__, e.isSilent()))
        for i in self.getContent():
            events.extend(i.getEventQueue())

        return events

    def getNextTimestamp(self):
        """
        Return the event with lowest time.

        :return: the event that occures next
        :rtype: event.Event
        """
        e = self.getEvents()
        if len(e) == 0:
            return None
        next = e[0].getTime()
        for i in e:
            t = i.getTime()
            if t < next:
                next = t
        return next

    def getGlobalTime(self):
        """
        Return the time of the root.

        :return: the time of the model an instance belongs to
        :rtype: dt.datetime
        """
        return self.getRoot().getTime()

    def getRoot(self):
        """."""
        uc = self
        while uc.getParent() is not None:
            uc = uc.getParent()
        return uc

# # # # # # # # Set and Get for instance parameters #

    def setEvents(self, events):
        """
        Set events of this Instance.

        :param events: the instances new events
        :type events: list of event.Event
        """
        self.events = events

    def getEvents(self):
        """
        Return events of this Instance.

        :return: event list of this instance
        :rtype: list of event.Event
        """
        return self.events

    def setContent(self, content):
        """
        Set content array of object.

        :param content: the new content
        :type content: list
        """
        self.content = content

    def getContent(self):
        """
        Return the content of the object.

        :return: an instances content
        :rtype: list
        """
        return self.content

    def setParent(self, Parent):
        """
        Set parent of Object.

        :param Parent: the parent node of an instance
        :type parent: instance of a class
        """
        self.parent = Parent

    def getParent(self):
        """
        Return the parent of the object.

        :return: the parent of an instance
        :rtype: object
        """
        return self.parent

    def setTime(self, value):
        """
        Set the local time of the object.

        :param value: the instances new timestamp
        :type value: dt.datetime
        """
        self.time = value

    def getTime(self):
        """
        Return the local time of the object.

        :return: the instances timestamp
        :rtype: dt.datetime
        """
        return self.time
