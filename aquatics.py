"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import pickle
import random
from abc import ABC, abstractmethod

import numpy as np

import feed
import simObject as so
import water

from DFD_WhiteLegShrimp import power


class Population(so.SimObject):
    """
    Initialize a population of aquatics.

    :param species: the species of the population
    :type species: class
    :param size: the number of animals in the population
    :type size: int
    :param weight: the average initial weight (in gramm) of a single animal
    :type weight: float
    :param targetWeight: the average weight (in gramm) at which the
        population is reduced
    :type targetWeight: int
    :param time: timestamp when instance was created
    :type time: dt.datetime
    """

    def __init__(self, species, size, startWeight, targetWeight,
                 time, stockingDensity, wallet):
        super().__init__(time)
        assert(issubclass(species, Aquatic))

        self.species = species
        self.size = size
        self.startWeight = startWeight
        self.targetWeight = targetWeight
        self.stockingDensity = stockingDensity

        """For the distribution of weights it is assumed that the fingerlings
        are sorted by the source of supply. Furthermore it is assumed that
        the weight distribution of the sorted fingerlings correspond to a
        normal distribution with a moderate coefficient of variation of 10%,
        which results in a standard-deviation of 0.1 times the startWeight."""
        self.weights = np.random.normal(startWeight, 0.1 * startWeight, self.size)

        self.deaths = 0

        self.data["avgWeights"] = [(time, self.getAverageWeight())]
        self.data["sumWeights"] = [(time, sum(self.weights.copy()))]
        self.data['popSize'] = [(time, size)]
        self.data['mortality'] = [(time, 0)]

        wallet.purchase(time, "Fingerling", size)
        # self.mortalityRate = species.getMortalityRate()

    def updateEvents(self):
        """Create new events."""
        w = self.getWater()
        if self.getSize() > 0:
            if any(isinstance(f, feed.Feed) for f in w.content):
                if not self.eventExists(self.feedingTime):
                    self.newEvent(self.feedingTime)
        if self.reducable():
            t = self.getParent().getParent()
            if not t.eventExists(t.reducePopulation):
                t.newEvent(t.reducePopulation, [self])
        if self.getSize() == 0:
            t = self.getParent().getParent()
            if not t.eventExists(t.fillEmUp):
                t.newEvent(t.fillEmUp, [self])

        super().updateEvents()

    def reducable(self, n=10):
        """
        Return True if heaviest n fish are >= self.targetWeight

        :returns: s.o.
        :rtype: bool
        """
        if len(self.getWeights()) >= n:
            s = sorted(self.getWeights())[-n:]
        else:
            s = self.getWeights()

        if len(s) > 0 and all([x >= self.targetWeight for x in s]):
            return True
        return False

    def feedingTime(self):
        self.liveOrDie()
        self.eat()
        self.grow()

        time = self.getGlobalTime()
        self.data["avgWeights"].append((time, self.getAverageWeight()))
        self.data["sumWeights"].append((time, self.getTotalWeight()))
        self.data["popSize"].append((time, self.getSize()))
        self.data['mortality'].append((time, self.getMortalityPercentage()))

        return ("eaten, grown, and died")

    def eat(self):
        """Let the population eat."""
        w = self.getWater()
        w.setContent([f for f in w.getContent() if not isinstance(f, feed.Feed)])
        # food is deleted from water instead of going into fish stomach
        # since we dont do stomach simulation yet. grow() will just
        # assume that the DFD is equal to stomach content

    def grow(self):
        """Let the population grow according to individual feed amount
        and FCR."""
        # Update weights according to FCR and DFD
        for i, w in enumerate(self.getWeights()):
            gain = (1 / self.species.getFCR(w)) * self.species.getDFD(w)
            self.weights[i] += gain

    def liveOrDie(self):
        """This function simulates mortality of the population"""
        if random.uniform(0, 1) <= self.species.getMortalityRate(
                self.stockingDensity):
            # Delete 0.5% of the initial population
            if (self.size / 200) >= 1:
                for i in range(int(self.size / 200)):
                    self.weights = self.weights[:-1]
                    self.size -= 1

            elif (self.deaths + (self.size / 200)) >= 1:
                self.deaths += (self.size / 200) - 1
                self.weights = self.weights[:-1]
                self.size -= 1
            else:
                self.deaths += (self.size / 200)

    def getSize(self):
        """
        Return the size.

        :returns: the number of animals in the population
        :rtype: int
        """
        return self.size

    def getWeights(self):
        """
        Return the weights.

        :returns: the weights of the animals in the population
        :rtype: list of float
        """
        return self.weights

    def getTargetWeight(self):
        """
        Return the target weight.

        :returns: the target weight of the population
        :rtype: float
        """
        return self.targetWeight

    def getAverageWeight(self):
        """
        Return the average weight of the animals.

        :returns: the average weight
        :rtype: float
        """
        return sum(self.weights) / self.size

    def getTotalWeight(self):
        """
        Return the total weight of the animals.

        :returns: the total weight
        :rtype: float
        """
        return sum(self.weights)

    def getMortalityPercentage(self):
        """
        Return the mortality rate at the current time in percent of the initial population size.
        :returns: the mortality rate
        :rtype: float
        """
        InitialFishpop = self.data["popSize"][0][1]
        FishInTank = self.data["popSize"][-1][1]
        SoldFish = self.getParent().getParent().numberSoldFish
        Difference = InitialFishpop - (FishInTank + SoldFish)
        MortalityRate = Difference / (InitialFishpop / 100)

        # print("Mortalityrate = ", MortalityRate, "%")
        return MortalityRate

    def getVolume(self):
        """
        Return the total volume of the population.

        :returns: the total volume of the population.
        :rtype: float
        """
        density = 1000
        # Density of aquatics is estimated to be 1000 kg/m^3. Volume = mass/density
        return sum(self.weights) / density

    def getWater(self):
        """
        Return water instance (parent) that the population is in.

        :return: the water instance containing this population
        :rtype: water.Water
        """
        w = self.getParent()
        assert(isinstance(w, water.Water)), (
            "Expected a water instance, got " + str(type(w)) + " instead"
        )
        return w

    def getDFDs(self):
        species = self.species
        return [species.getDFD(i) for i in self.weights]

    def getFCRs(self):
        species = self.species
        return [species.getFCR(i) for i in self.weights]


class Aquatic(ABC):
    """Class Aquatic."""

    @abstractmethod
    def getFCR():
        pass

    @abstractmethod
    def getDFD():
        pass

    @abstractmethod
    def getMortalityRate():
        pass


class AfricanCatfish(Aquatic):
    """Class AfricanCatfish."""

    FCR = pickle.load(open(
        ".Pickles/pickle_FCR_AfricanCatfish", 'rb'))

    DFD = pickle.load(open(
        "pickle_DFD_AfricanCatfish", 'rb'))

    Mortality = pickle.load(open(
        ".Pickles/pickle_Mortality_StockingDensity_Catfish",
        'rb'))

    @staticmethod
    def getFCR(weight):
        """
        Returns the feed conversion ratio of an individual.
        The feed conversion ratio is defined as the ratio of inputs to outputs.
        In this case the input is feed and the output is growth.

        :param weight: weight of individual in kilogramm
        :type weight: float
        :return: the current FCR for the specific individual
        :rtype: float
        """
        return float(AfricanCatfish.FCR(weight))

    @staticmethod
    def getDFD(weight):
        """
        Returns the daily feed demand of an individual in kilogramm.

        :param weight: weight of individual in kilogramm
        :type weight: float
        :return: the current daily feed demand for the specific individual
        :rtype: float
        """
        # DFD as a percentage of the individual's bodyweight
        relativeDFD = float(AfricanCatfish.DFD(weight))
        # DFD as a total value in kilogramm
        absoluteDFD = weight / 100 * relativeDFD
        return absoluteDFD

    @staticmethod
    def getMortalityRate(StockingDensity):
        """
        Returns the MortalityRate of a population based on the StockingDensity.
        MortalityRate in this specific case is defined as the chance in % that
        0.5% of the population dies per day.
        For African Catfish the mortality rate is based on data from:
        "Palm et al. 2018 - Proportional up scaling of African catfish
        (Clarias gariepinus Burchell, 1822) commercial recirculating aquaculture
        systems disproportionally affects nutrient dynamics"
        Table 2, Table 3, Fish cohort 6

        :param StockingDensity: StockingDensity of the population in Kg/m^3
        :type StockingDensity: float
        :return: the chance of 0.5% of population size dying
        :rtype: float
        """
        return float(AfricanCatfish.Mortality(StockingDensity))


class WhiteLegShrimp(Aquatic):
    """Class WhitelegShrimp."""

    FCR = pickle.load(open(
        ".Pickles/pickle_FCR_WhiteLegShrimp", 'rb'))

    DFD = pickle.load(open(
        "pickle_DFD_WhiteLegShrimp", 'rb'))

    # Mortality = pickle.load(open(
    #     ".Pickles/pickle_Mortality_StockingDensity_WhiteLegShrimp",
    #     'rb'))
    # This is relevant once a dataset for mortality based on stocking density is available

    @staticmethod
    def getFCR(weight):
        """
        Returns the feed conversion ratio of an individual.
        The feed conversion ratio is defined as the ratio of inputs to outputs.
        In this case the input is feed and the output is growth.

        :param weight: weight of individual in kilogramm
        :type weight: float
        :return: the current FCR for the specific individual
        :rtype: float
        """
        return float(WhiteLegShrimp.FCR(weight))

    @staticmethod
    def getDFD(weight):
        """
        Returns the daily feed demand of an individual in kilogramm.

        :param weight: weight of individual in kilogramm
        :type weight: float
        :return: the current daily feed demand for the specific individual
        :rtype: float
        """
        # DFD as a percentage of the individual's bodyweight
        relativeDFD = float(WhiteLegShrimp.DFD(weight))
        # DFD as a total value in kilogramm
        absoluteDFD = weight / 100 * relativeDFD
        return absoluteDFD

    @staticmethod
    def getMortalityRate(StockingDensity):
        """
        Returns the MortalityRate of a population based on the StockingDensity.
        MortalityRate in this specific case is defined as the chance in % that
        0.5% of the population dies per day.
        For WhiteLegShrimp there currently is no dataset available that allows
        a correlation between StockingDensity and mortalityrate.
        The here considered fix mortalityrate (of 2.3% mortality per week) is
        based on:
        "Yao, Si Qi, 2019 - Assessment and modeling of whiteleg shrimp production
        in a low-salinity recirculating aquaculture system"
        Batch 4 ST1/BT
        See /.Pickles/MortalityrateWhiteLegShrimp for more information.
        Although currently the mortalityrate is fix and independent from the
        StockingDensity, this function remains in place in order to replace
        the simplified model with a more sophisticated one.
        :param StockingDensity: StockingDensity of the population in Kg/m^3
        :type StockingDensity: float
        :return: the chance of 0.5% of population size dying
        :rtype: float
        """
        return 0.6572
