"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import datetime as dt
import os
import pickle
import random

import aquatics as aq
import electrical as ec
import event
import feedDispenser as fd
import filtration
import heater
import inlet
import outlet
import plots
import pump
import simObject as so
import tank
import wallet
import water


class Model(so.SimObject):
    """
    Set up the simulation environment.

    :param time: the time that the simulation starts
    :type time: dt.datetime
    :param configuration: confiuration for the simulation
    :type configuration: configuration.Configuration
    """

    showSilentEvents = False

    def __init__(self, configuration, time=dt.datetime.now()):
        """Initialize Model instance."""
        random.seed(42)
        super().__init__(time)
        self.configuration = configuration
        self.id = configuration.id
        self.currentTick = 0
        self.duration = configuration.simDuration
        self.start = self.getTime()
        self.wallet = wallet.Wallet(self.start, configuration.prices)

        self.newEvent(lambda x: x, ["Started simulation"],
                      dt.timedelta(microseconds=1))

        tanks = self.initTanks(configuration.tanks, configuration.tapWaterTemp)

        size = self.calculateSumpTankSize(tanks)
        s = tank.Sumptank("Sumptank", time, size[0], size[1], size[2])
        w = water.Water(s.getCapacity(), configuration.tapWaterTemp, time,
                        self.wallet, self.configuration.salinity)

        s.addContent(w)

        h = heater.Heater("Heater", time, configuration.targetWaterTemp, self.wallet)

        # It is assumed that the main pump moves 1.9 times the total water
        # volume of the facility per hour. The height difference between the
        # sump tank and the fish tanks is assumed to be 1.5m

        p = pump.Pump("Pump - Main Circuit", time, self.wallet, 1.5,
                      sum(e.getCapacity() for e in tanks) * 1.9, s, *tanks)
        i = inlet.Inlet(
            s, h, configuration.tapWaterTemp, configuration.targetWaterTemp,
            self.configuration.salinity, time, self.wallet)
        o = outlet.Outlet(s, time, configuration.dailyDiscardRate, self.wallet)

        size = self.calculateBioFilterSize(tanks)
        bf = filtration.BioFilter(
            "BiologicalFilter", time, size[0], size[1], size[2], target=None)
        w = water.Water(bf.getCapacity(), configuration.tapWaterTemp, time,
                        self.wallet, self.configuration.salinity)
        bf.addContent(w)

        # It is assumed that the biofilter pump moves 10 times the total water
        # volume of the biofilter per hour. The height difference between the
        # sump tank and the top of the biofilter is assumed to be 3m
        p2 = pump.Pump("Pump - BioFilter", time, self.wallet,
                       3.0, bf.getCapacity() * 10, s, bf)

        size = self.calculateMechFilterSize(tanks)
        mf = filtration.MechanicalFilter(
            "MechanicalFilter", time, size[0], size[1], size[2], target=None)
        w = water.Water(mf.getCapacity(), configuration.tapWaterTemp, time,
                        self.wallet, self.configuration.salinity)
        mf.addContent(w)

        self.addContent(i, o, s, p, mf, h, bf, p2)

        if configuration.salinity > 0:
            flowrate = self.calculateSkimmerFlowRate(tanks)
            size = self.calculateSkimmerSize(flowrate)
            sk = filtration.Skimmer(
                "ProteinSkimmer", time, size[0], size[1], size[2], target=None)
            w = water.Water(sk.getCapacity(), configuration.tapWaterTemp, time,
                            self.wallet, self.configuration.salinity)
            sk.addContent(w)
            p3 = pump.AirPump("ProteinSkimmer", time, self.wallet,
                              flowrate, s, sk)
            self.addContent(sk, p3)

    def update(self):
        """
        Update the entire Model.

        This is the global update function.
        """
        executed = []
        t = self.getTime()
        executed = super().update()
        self.updateEvents()
        assert t < self.getTime(), "Monotonic criterion violated!"

        if not event.Event.hideAll:
            if any([not e.silent for e in executed]):
                print(self.getQueueString())
        return executed

    def simulate(self, showSilentEvents=False):
        """
        Run the simulation.

        :param duration: the duration to simulate in days
        :type duration: int
        :param showSilentEvents: wether or not to show silent events
        :type showSilentEvents: bool
        :returns: the ticks and data for the plots
        :rtype: list of dt.timdelta and list of some sort
        """
        Model.showSilentEvents = showSilentEvents

        ticks = []  # for debug purpose only
        executed = []  # for debug purpose only
        durations = []  # for debug purpose only

        beforeUpdate = dt.datetime.now()

        while (self.getTime() - self.start).days < self.duration:
            events = self.update()
            afterUpdate = dt.datetime.now()

            ticks.append((self.getTime() - self.start).total_seconds() / 86400.0)
            executed.extend(events)
            durations.append((afterUpdate - beforeUpdate).total_seconds())

            beforeUpdate = afterUpdate
            self.currentTick += 1

        # print("Finished in " + str((afterUpdate - self.start).total_seconds()) + " seconds")

        numbers = {
            "Total Energy Consumption in kWh": sum([x.electricMeter for x in self.getElectricals()]),
            "Total Feed Consumption in kg": sum([x.meter for x in self.getFeedDispensers()]),
            "Total amount of sold fish in kg": sum([x.fishmeter for x in self.getTanks()]),
            "Total Fresh Water Consumption m^3": self.getInlet().meter,
            "Total Waste Water Consumption m^3": self.getOutlet().meter,
            "Total Salt Consumption kg": self.getInlet().saltmeter,
        }

        return numbers
        # plots.resourcePlots(ticks, executed, durations)

    def initTanks(self, tankConfigs, WaterTemp):
        """Create tank instances from configuration tuple."""
        tanks = []
        time = self.getTime()

        # (name, (radius, heigth), species, stockingDensity, startWeight, targetWeight)
        for i, t in enumerate(tankConfigs):
            if t['TankDiameter'] is None:
                newTank = tank.CuboidTank(t['Reference'], time, t['TankWidth'],
                                          t['TankDepth'], t['TankHeight'], self.wallet)
            else:
                newTank = tank.CylinderTank(
                    t['Reference'], time, t['TankDiameter'], t['TankHeight'], self.wallet)
            w = water.Water(newTank.getCapacity(), WaterTemp, time,
                            self.wallet, self.configuration.salinity)

            if t['Species'] == "African Catfish":
                species = aq.AfricanCatfish
            elif t['Species'] == "WhiteLegShrimp":
                species = aq.WhiteLegShrimp

            size = self.calculateFishStock(
                species, newTank, t['StockingDensity'], t['TargetHarvestingWeight'])

            pop = aq.Population(species, size, t['StartSpecieWeight'],
                                t['TargetHarvestingWeight'], time, t['StockingDensity'], self.wallet)
            f = fd.FeedDispenser("FeedDispenser" + str(i), newTank, time, self.wallet)
            w.addContent(pop)
            newTank.addContent(w)
            self.addContent(newTank, f)
            tanks.append(newTank)
        return tanks

    def calculateFishStock(self, species, tank, density, targetWeight):
        """
        Calculate the number of fish that fits into the tank.
        This is calculated regarding the capacity, stocking Density
        and the targeted average fish weight depending on the species.
        For WhiteLegShrimp the stocking density is usually given in
        animals/m², since shrimp prefer to stay near the bottom of the tank.

        :param species: the aquatic species to be cultivated
        :type species: class
        :param tank: the target container
        :type tank: con.container
        :param density: the stocking density
        :type density: float
        :param targetWeight: the average weight per animal when ready to sell
        :type targetWeight: float
        :return: the number of animals that fit into the container
            regarding the constraints
        :rtype: int
        """
        capacity = tank.getCapacity()  # n volumen tank
        totalWeight = capacity * density  # x*n gewicht pro n volumen tank
        if species == aq.AfricanCatfish:
            return int(totalWeight / targetWeight)  # anzahl fische
        if species == aq.WhiteLegShrimp:
            return int(density * tank.getSurface())

    def calculateBioFilterSize(self, tanks):
        """
        Calculates the biological filtration volume depending on the volume
        of the tanks in the system and the average of their respective
        stocking densities.
        """
        relativeBioFilterVol = pickle.load(open(
            ".Pickles/pickle_BioFilterVol_StockingDensity",
            'rb'))
        totalTankVolume = sum(e.getCapacity() for e in tanks)
        totalBioFilterVol = relativeBioFilterVol(
            self.calculateWeightedStockingDensity(tanks)) / 100 * totalTankVolume
        # It is assumed that the biological filtration should have a fix height
        # of 0.1m and both width and depth have the same value.
        Height = 0.1
        Width = (totalBioFilterVol / Height) ** (1 / 2.0)
        return (Width, Width, Height)

    def calculateSkimmerSize(self, flowrate):
        """
        Calculates the protein skimmer volume depending on the volume
        of the tanks in the system.
        It is assumed that the volume of the skimmer is 1.5 times the
        flow rate per minute.
        """
        Volume = 1.5 * (flowrate / 60)
        # It is assumed that the skimmer should have a fix height
        # of 2m and both width and depth have the same value.
        Height = 2.0
        Width = (Volume / Height) ** (1 / 2.0)
        return (Width, Width, Height)

    def calculateSkimmerFlowRate(self, tanks):
        """
        Calculates the flow rate of the air pump that runs the protein skimmer.
        It is assumed that the flowrate is 0.5 times the total water volume
        of the tanks.
        """
        totalTankVolume = sum(e.getCapacity() for e in tanks)
        flowrate = totalTankVolume * 0.5
        return flowrate

    def calculateSumpTankSize(self, tanks):
        """
        Calculates the sumptank volume depending on the volume
        of the tanks in the system and the average of their respective
        stocking densities.
        """
        relativeSumpVol = pickle.load(open(
            ".Pickles/pickle_SumpTankVol_StockingDensity",
            'rb'))
        totalTankVolume = sum(e.getCapacity() for e in tanks)
        totalSumpVol = relativeSumpVol(
            self.calculateWeightedStockingDensity(tanks)) / 100 * totalTankVolume
        # It is assumed that the sumptank should have a fix height of 0.7m
        # and both width and depth have the same value.
        Height = 0.7
        Width = (totalSumpVol / Height) ** (1 / 2.0)
        return (Width, Width, Height)

    def calculateMechFilterSize(self, tanks):
        """
        Calculates the mechanical filtration volume depending on the volume
        of the tanks in the system and the average of their respective
        stocking densities.
        """
        relativeMechFilterVol = pickle.load(open(
            ".Pickles/pickle_MechFilterVol_StockingDensity",
            'rb'))
        totalTankVolume = sum(e.getCapacity() for e in tanks)
        totalMechFilterVol = relativeMechFilterVol(
            self.calculateWeightedStockingDensity(tanks)) / 100 * totalTankVolume
        # It is assumed that the mechanical filtration should have a fix height
        # of 0.9m and both width and depth have the same value.
        Height = 0.9
        Width = (totalMechFilterVol / Height) ** (1 / 2.0)
        return (Width, Width, Height)

    def calculateWeightedStockingDensity(self, tanks):
        """
        Calculates a weighted stocking density for multiple tanks based on
        their water volume and stocking density.
        """
        Volumes = []
        StockingDensities = []
        for i in tanks:
            Volumes.append(i.getCapacity())
            StockingDensities.append(i.getPopulation().stockingDensity)
        return (sum([a * b for a, b in zip(Volumes, StockingDensities)])
                / sum(Volumes))

    def makePlots(self):
        """Create and save plots."""

        os.makedirs("./SVG/" + str(self.id) + "/wallet/", exist_ok=True)

        plots.plotTanks(self)

        plots.plotWallet(self)

        plots.plotElectricals(self)

    def getElectricals(self):
        """Return all electricals in a list."""
        return sorted([x for x in self.getContent() if isinstance(x, ec.Electrical)],
                      key=lambda x: x.name)

    def getConfiguration(self):
        """
        Getter Function for Model Configurationuration
        """
        return self.configuration

    def getNextTimestamp(self):
        """
        Return the timestamp of next event.
        In Model Class all upcoming events are considered.

        :return: the minimum time of all upcoming events
        :rtype: dt.datetime
        """
        self.setTime(super().getNextTimestamp())
        times = [t for t in self.getObjectTimes() if t is not None]
        if times == []:
            exit("no more events")
        return min(times)

    def getEventQueue(self):
        """
        Return all upcoming events in correct order.

        :return: all upcoming events
        :rtype: list of event.Event tuples
        """
        events = super().getEventQueue()
        return sorted(events, key=lambda x: x[1])

    def getQueueString(self):
        """
        Return pretty printed upcoming events.

        :return: upcoming events in correct order
        :rtype: string
        """
        Queue = ""
        markTime = None
        for i in self.getEventQueue():
            if i[3] and not Model.showSilentEvents:
                continue

            Queue += i[0] + '\t'

            if len(i[0]) <= 4:
                Queue += '\t'

            if len(i[0]) <= 8:
                Queue += '\t'

            if len(i[0]) <= 12:
                Queue += '\t'

            if i[3]:
                if i[1] == markTime or markTime is None:
                    Queue += "\033[1;95;7m"  # 1;34
                else:
                    Queue += "\033[95m"
            else:
                if i[1] == markTime or markTime is None:
                    Queue += "\033[1;31;7m"  # 1;34
                    markTime = i[1]
                else:
                    Queue += "\033[1;31m"  # 1;34
            Queue += (i[1].strftime('%d/%m/%y %H:%M:%S') +
                      '\t' + i[2] + "\033[0m\n")

        return Queue + '\n'

    def getTank(self, name):
        """
        Return a tank with matching name.

        If there isn't EXACTLY one tank with matching name,
        simulation will crash

        :param name: the name of the tank
        :type name: string
        :return: the tank with the matching name
        :rtype: tank.Tank
        """
        t = [x for x in self.content if isinstance(x, tank.Tank)
             and x.name == name]
        assert len(t) == 1, str(len(t)) + " matches found for " + name
        return t[0]

    def getTanks(self):
        """
        Return all Tanks in models content

        :return: the tank with the matching name
        :rtype: tank.Tank
        """
        t = [x for x in self.content if isinstance(x, tank.Tank)]
        return t

    def getMechanicalFilter(self, name):
        """
        Return a tank with matching name.

        If there isn't EXACTLY one tank with matching name,
        simulation will crash

        :param name: the name of the tank
        :type name: string
        :return: the tank with the matching name
        :rtype: tank.Tank
        """
        t = [x for x in self.content if isinstance(x, filtration.MechanicalFilter)
             and x.name == name]
        assert len(t) == 1
        return t[0]

    def getTotalWaterVolume(self):
        """
        Return sum of all water.

        :returns: the sum of all water
        :rtype: int
        """

        TotalWaterVolume = 0
        instances = self.getInstances()
        for i in instances:
            if isinstance(i, water.Water):
                TotalWaterVolume += i.amount
        return TotalWaterVolume

    def getPump(self, name):
        """
        Return a pump with matching name.

        :param name: the name of the pump
        :type name: string
        :return: the pump with the matching name
        :rtype: pump.Pump
        """
        p = [x for x in self.content if isinstance(x, pump.Pump)
             and x.name == name]
        assert len(p) == 1
        return p[0]

    def getSumptank(self, name):
        """
        Return a sumptank with matching name.

        :param name: the name of the sumptank
        :type name: string
        :return: the sumptank with the matching name
        :rtype: pump.sumptank
        """
        s = [x for x in self.content if isinstance(x, pump.Sumptank)
             and x.name == name]
        assert len(s) == 1
        return s[0]

    def getFeedDispensers(self):
        """
        Return all FeedDispensers.

        :return: all FeedDispensers
        :rtype: list of feedstorage.FeedDispenser
        """
        s = [x for x in self.getContent() if isinstance(x, fd.FeedDispenser)]
        return s

    def getInlet(self):
        """
        Return the inlet.

        :return: the Inlet
        :rtype: inlet.Inlet
        """
        s = [x for x in self.content if isinstance(x, inlet.Inlet)]
        return s[0]

    def getOutlet(self):
        """
        Return the Outlet.

        :return: the Outlet
        :rtype: outlet.Outlet
        """
        s = [x for x in self.content if isinstance(x, outlet.Outlet)]
        return s[0]

    def getWallet(self):
        """Return the wallet.
        :return: the wallet
        :rtype: wallet.Wallet
        """
        return self.wallet
