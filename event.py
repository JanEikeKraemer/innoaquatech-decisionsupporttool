"""
    This program simulates aquaculture facilities and gives an overview of the respective resource consumption.
    It was written as part of the Interreg project "InnoAquaTech" at the University of Rostock.

    Copyright (C) 2019 Jan Eike Kraemer <jan.eike.kraemer@protonmail.com>, Hendrik Evert Pfennig <hen.pfen@web.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import datetime as dt


class Event:
    """
    Event struct for all Objects.

    :param callback: the function to execute.
        Should return a string for logging
    :type callback: function
    :param args: the parameters for callback function
    :type args: list of objects
    :param time: the time that callback will be executed.
    :type time: dt.datetime
    :param silent: wether the event is logged or not
    :type silent: bool
    """

    hideAll = True

    def __init__(self, callback, args, time, silent=False):
        """Initialize event with a time and a callback function."""
        self.time = time
        self.callback = callback
        self.args = args
        self.silent = silent

    def __str__(self):
        """Properly show event instance."""
        return str(self.callback.__name__) + "\n" + str(self.args)

    def execute(self):
        """Execute callback function with arguments."""
        log = self.callback(*self.args)
        if not Event.hideAll and not self.silent:
            print("\033[1;32m" + log + "\033[0m\n")

    def isSilent(self):
        """
        Return wether event is silent or not.

        :return: True if event is silent, False otherwise
        :rtype: bool
        """
        return self.silent

    def getTime(self):
        """
        Return time of the event.

        :return: the time of the event
        :rtype: dt.datetime
        """
        return self.time


class Periodic(Event):
    """
    Structure for periodic event.

    :param callback: the function to execute.
        Should return a string for logging
    :type callback: function
    :param args: the parameters for callback function
    :type args: list of objects
    :param time: the time that callback will be executed.
    :type time: dt.datetime
    :param period: the period in which callback is repeated
    :type period: dt.timedelta
    :param silent: wether the event is logged or not
    :type silent: bool
    """

    def __init__(self, callback, args, time, period, silent=False):
        """Initialize periodic time event."""
        super().__init__(callback, args, time, silent)
        self.period = period
        assert isinstance(self.period, dt.timedelta), (
            "expected period to be timedelta type\n" +
            "got " + str(type(period)) + " instead"
        )

    def nextOccurance(self):
        """
        Calculate the next date this event will trigger.

        :return: the next time the event will trigger
        :rtype: dt.datetime
        """
        self.time += self.period
        return self.time


class Cyclic(Event):
    """
    Structure for cyclic event.

    :param callback: the function to execute.
        Should return a string for logging
    :type callback: function
    :param args: the parameters for callback function
    :type args: list of objects
    :param time: the time that callback will be executed.
        Default is datetime.now
    :type time: dt.datetime
    :param cycle: the cycle of times to execute callback
    :type cycle: list of dt.timedelta
    :param silent: wether the event is logged or not
    :type silent: bool
    """

    def __init__(self, callback, args, time, cycle, silent=False):
        """Initialize periodic time event."""
        super().__init__(callback, args, time, silent)
        self.cycle = cycle
        assert isinstance(self.cycle, list), (
            "expected period to be list\n" +
            "got " + str(type(cycle)) + " instead"
        )
        for i in cycle:
            assert isinstance(i, dt.timedelta), (
                "expected cycle to be timedelta type\n" +
                "got " + str(type(i)) + " instead"
            )

        def periodGenerator(timedeltas):
            while True:
                for i in timedeltas:
                    yield i

        self.cycle = periodGenerator(cycle)
        self.time += next(self.cycle)

    def nextOccurance(self):
        """
        Calculate the next date this event will trigger.

        :return: the next time the event will trigger
        :rtype: dt.datetime
        """
        self.time += next(self.cycle)
        return self.time
